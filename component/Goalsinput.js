import React,{useState} from 'react';
import{View,TextInput,Button,StyleSheet,Modal} from 'react-native';
import Goalitem from './Goalsitem';

const GoalsInput =props=>{
    const [enterGoal,setEnterGoal]=useState('');

    const handelchangeInput=(enterText)=>{
        setEnterGoal(enterText);
      }
      const handeladdEvent= () =>{
        props.handelClick(enterGoal)
        setEnterGoal('')

      }
    return(
        <Modal visible={props.visablty} animationType="slide">
        <View style={styles.innerView}>
       <TextInput
       placeholder="Your Goal"
       style={styles.textView}
       onChangeText={handelchangeInput}
       value={enterGoal}
       />
       <View style={styles.ButtonView}>
         <View style={styles.Button}>
       <Button  title='ADD' onPress={handeladdEvent}/>
       </View>
       <View style={styles.Button}>
       <Button  title='Cancel' color='red' onPress={props.cancleEvent}/>
       </View>
       </View>
       </View>
        </Modal>

    )
}
const styles=StyleSheet.create({
    textView:{
        width:'80%',
        borderBottomColor: 'black',
        borderWidth : 1,
        padding: 10,
        marginBottom:10
      },
      innerView:{
        justifyContent:'center',
        alignItems:'center',
        flex:1
      },
      ButtonView:{
        flexDirection:'row',
        width:'60%',
        justifyContent:'space-around'
      },
      Button:{
        width:'40%'
      }

})
export default GoalsInput;