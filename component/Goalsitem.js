import React from 'react';
import{View,Text,StyleSheet,TouchableOpacity} from 'react-native';

const Goalitem=props=>{
    return(
        <TouchableOpacity activeOpacity={0.8} onPress={()=>props.ondelete(props.id)}>
        <View  style={styles.Itemview}>
        <Text > {props.title} </Text>
        </View>
        </TouchableOpacity>

    )
}
const styles=StyleSheet.create({
    Itemview:{
        marginVertical:10,
        padding:10,
        backgroundColor:'#ccc',
        borderColor:'black',
        borderWidth:1
      }

})
export default Goalitem;