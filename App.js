/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React,{useState} from 'react';
import {
  Button,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput ,
  FlatList
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Goalsitem from './component/Goalsitem';
import GoalsInput from './component/Goalsinput';
const App = () => {
  const [courceGoal,setCouseGoal]=useState([]);
  const [isvisable,setisvisable]=useState(false);
 
  const handelClick=(GoalTitel)=>{
    if(GoalTitel!==''){
    setCouseGoal(currentGoal=> [...currentGoal,{
      key:Math.random().toString(),
      value:GoalTitel
    }])
  }
    setisvisable(false);
  }
  const cancleEvent=()=>{
    console.log("test")
    setisvisable(false);
  }
const ondelete=Goalid=>{
  setCouseGoal (currentGoal =>{
    return currentGoal.filter((goal)=>goal.key!==Goalid)
  });
}
  return (
    <>
     <View style={styles.Root}>
     <Button title='Add New Goals' onPress={()=>setisvisable(true)}/>
       <GoalsInput 
       visablty={isvisable}
       handelClick={handelClick}
       cancleEvent={cancleEvent}
        />

       <FlatList
       data={courceGoal}
       renderItem={itemData=>(
        <Goalsitem id={itemData.item.key} ondelete={ondelete} title={itemData.item.value}/>
       )}       
       />
     </View>
    </>
  );
};

const styles = StyleSheet.create({
  Root:{
    padding:30
  },
  
});

export default App;
